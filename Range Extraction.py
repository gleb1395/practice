# A format for expressing an ordered list of integers is to use a comma separated list of either
#
# individual integers
# or a range of integers denoted by the starting integer separated
# from the end integer in the range by a dash, '-'.
# The range includes all integers in the interval including both endpoints.
# It is not considered a range unless it spans at least 3 numbers. For example "12,13,15-17"
# Complete the solution so that it takes a list of integers in increasing
# order and returns a correctly formatted string in the range format.
#
# Example:
#
# solution([-10, -9, -8, -6, -3, -2, -1, 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20])
# # returns "-10--8,-6,-3-1,3-5,7-11,14,15,17-20"

def solution(args):
    res_lst = []
    count = 0
    index = 0
    while args:
        div = abs(args[index] - args[index + 1])
        if div == 1:
            for i in range(0, len(args)):
                if index < len(args) - 1 and abs(args[index] - args[index + 1]) == 1:
                    index += 1
                    count += 1
                elif count == 1:
                    res_lst.append(str(args[index-1]) + ',')
                    del args[index-1]
                    count = 0
                    index = 0
                    break
                else:
                    res_lst.append(str(args[index - count]) + '-' + str(args[index]) + ',')
                    del args[:index + 1]
                    count = 0
                    index = 0
                    break
        if div >= 2:
            res_lst.append(str(args[index]) + ',')
            del args[index]
            index = 0
    return ' '.join(res_lst)


print(solution([-10, -9, -8, -6, -3, -2, -1, 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20]))
