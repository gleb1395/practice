# array = [[1,2,3],
#          [4,5,6],
#          [7,8,9]]
# snail(array) #=> [1,2,3,6,9,8,7,4,5]
# For better understanding, please follow the numbers of the next array consecutively:
#
# array = [[1,2,3],
#          [8,9,4],
#          [7,6,5]]
# snail(array) #=> [1,2,3,4,5,6,7,8,9]
# img.png - This image will illustrate things more clearly

def snail(snail_map):
    empty_lst = []
    while True:
        empty_lst.extend(snail_map.pop(0))
        if snail_map and snail_map[0]:
            for row in snail_map:
                empty_lst.append(row.pop(-1))
        if snail_map:
            empty_lst.extend(snail_map.pop()[::-1])
        if snail_map and snail_map[0]:
            for row in snail_map[::-1]:
                empty_lst.append(row.pop(0))

    return empty_lst


print(snail(
    [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]]
)
)
