# def new_format(string):
#    # code here
# assert (new_format("1000000") == "1.000.000")
# assert (new_format("100") == "100")
# assert (new_format("1000") == "1.000")
# assert (new_format("100000") == "100.000")
# assert (new_format("10000") == "10.000")
# assert (new_format("0") == "0")

def new_format(string):
    num_list = list(string)
    count_zero = 0
    index = -1
    format_list = []
    for i in range(len(num_list)):
        if num_list[index] == "0":
            count_zero += 1
            format_list.append(num_list[index])
            index -= 1
            if count_zero == 3:
                format_list.append(".")
                count_zero = 0
        else:
            format_list.append(num_list[index])
    format_list.reverse()
    format_list = ''.join(format_list)
    return format_list

assert (new_format("1000000") == "1.000.000")
assert (new_format("100") == "100")
assert (new_format("1000") == "1.000")
assert (new_format("100000") == "100.000")
assert (new_format("10000") == "10.000")
assert (new_format("0") == "0")
print("happy")
