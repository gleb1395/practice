# Write a function that takes in a string of one or more words,
# and returns the same string, but with all words that have five or more letters reversed
# (Just like the name of this Kata). Strings passed in will consist of only letters and spaces.
# Spaces will be included only when more than one word is present.
#
# Examples:
# "Hey fellow warriors"  --> "Hey wollef sroirraw"
# "This is a test        --> "This is a test"
# "This is another test" --> "This is rehtona test"

def spin_words(sentence: str) -> str:
    word_list = sentence.split(' ')
    list_words = []
    for word in word_list:
        if len(word) >= 5:
            list_words.append(word[::-1])
        else:
            list_words.append(word)
    result = ' '.join(list_words)
    return result

print(spin_words('and word a one reversed words Spaces be in all reversed like name like a of be function returns name a same string and and a same'))