# Move the first letter of each word to the end of it,
# then add "ay" to the end of the word. Leave punctuation marks untouched.
#
# Examples
# pig_it('Pig latin is cool') # igPay atinlay siay oolcay
# pig_it('Hello world !')     # elloHay orldway !
import string


def pig_it(text):
    list_words = []
    for word in text.split():
        if word in string.punctuation:
            list_words.append(word)
        else:
            list_words.append(word[1:] + word[0] + 'ay')
    return ' '.join(list_words)
